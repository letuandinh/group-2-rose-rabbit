export const stays = [
  {
    hotel: 'The Georges',
    checkInDate: '2020-04-04',
    checkOutDate: '2020-04-06',
    location: '11 N Main St, Lexington, VA',
    notes: ' Relaxed hotel that offers complimentary WiFi & parking',
  },
  {
    hotel: 'Hilton Norforlk The Main',
    checkInDate: '2020-04-06',
    checkOutDate: '2020-04-08',
    location: '100 E Main St,Norfolk, VA',
    notes: 'A 4-star hotel with wonderful ocean views',
  },
  {
    hotel: 'Comfort Inn',
    checkInDate: '2020-04-08',
    checkOutDate: '2020-04-10',
    location: '716 21st St,Virginia Beach, VA',
    notes:
      'Unfussy rooms with minifridges & microwaves, plus a seasonal outdoor pool, free parking & Wifi',
  },
  {
    hotel: 'The Berry Hill Resort & Conference Center',
    checkInDate: '2020-04-10',
    checkOutDate: '2020-04-11',
    location: '3105 River Road,South Boston, VA',
    notes:
      'Elegent hotel set in an 18th-century mansion offering refined quarters, plus a spa, dining & a pool',
  },
  {
    hotel: 'Park Inn',
    checkInDate: '2020-04-11',
    checkOutDate: '2020-04-16',
    location: '2007 Richmond Road,Williamsburg, VA',
    notes:
      'Budger option with a restaurant & a pool, plus complimentary breakfast, laundry services & Wifi',
  },
];
